package com.itbeta.aguinaldo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AguinaldoTestItbetaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AguinaldoTestItbetaApplication.class, args);
	}

}
