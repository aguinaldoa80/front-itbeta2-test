package com.itbeta.aguinaldo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itbeta.aguinaldo.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
	
}
