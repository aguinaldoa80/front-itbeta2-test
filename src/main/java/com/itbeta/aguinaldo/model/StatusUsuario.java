package com.itbeta.aguinaldo.model;

public enum StatusUsuario {

	B /* BLOQUEADO */, 
	A /* ATIVO */;
}
