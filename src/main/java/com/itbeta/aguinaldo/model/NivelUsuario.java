package com.itbeta.aguinaldo.model;

public enum NivelUsuario {

	A /* ADMIN */, 
	U /* USUARIO */, 
	G /* GESTOR */, 
	F /* FUNCIONÁRIO */;
	
}
