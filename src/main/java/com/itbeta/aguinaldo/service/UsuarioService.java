package com.itbeta.aguinaldo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itbeta.aguinaldo.model.Usuario;
import com.itbeta.aguinaldo.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	public Usuario cadastroUsuario(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	public Usuario atualizarUsuario(Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
}
