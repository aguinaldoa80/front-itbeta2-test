package com.itbeta.aguinaldo.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itbeta.aguinaldo.model.NivelUsuario;
import com.itbeta.aguinaldo.model.StatusUsuario;
import com.itbeta.aguinaldo.model.Usuario;
import com.itbeta.aguinaldo.repository.UsuarioRepository;
import com.itbeta.aguinaldo.service.UsuarioService;
import com.itbeta.aguinaldo.util.EmailUtil;


@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@GetMapping(value = "/all")
	public List<Usuario> listarUsuarios() {
		return usuarioRepository.findAll();
	}

	@PostMapping
	public ResponseEntity<Long> cadastroUsuario(@RequestBody Usuario usuario) {

		usuario.setCreated_at(LocalDateTime.now());
		usuario.setNome(usuario.getNome().toUpperCase());
		usuario.setNivelUsuario(NivelUsuario.A);
		usuario.setStatus(StatusUsuario.A);
		usuario.setEmail(usuario.getEmail().toLowerCase());
		usuario.setSenha("123456");
		Usuario usuarioSalvo = usuarioService.cadastroUsuario(usuario);
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioSalvo.getId());
	}

	@PutMapping("/{id}")
	public ResponseEntity<Long> atualizarUsuario(@PathVariable Long id, @RequestBody Usuario usuario) {
		Optional<Usuario> usuarioOld = usuarioRepository.findById(id);
		
		usuarioOld.get().setUpdated_at(LocalDateTime.now());
		usuarioOld.get().setNome(usuario.getNome().toUpperCase());
		usuarioOld.get().setNivelUsuario(usuario.getNivelUsuario().A);
		usuarioOld.get().setStatus(usuario.getStatus().A);
		usuarioOld.get().setEmail(usuario.getEmail().toLowerCase());
		usuarioOld.get().setSenha("123456");
		Usuario usuarioAtualizado = usuarioService.atualizarUsuario(usuarioOld.get());
		final String fromEmail = "aguinaldoa80";
		final String password = "ab115798";
		final String toEmail = "aguinaldojpo@hotmail.com"; // can be any email id 
		
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
		props.put("mail.smtp.port", "587"); //TLS Port
		props.put("mail.smtp.auth", "true"); //enable authentication
		props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS
		
                //create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		
		EmailUtil.sendEmail(session, toEmail,"TLSEmail Testing Subject", "TLSEmail Testing Body");
		
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioAtualizado.getId());

	}

}
